import {
  Switch,
  Route
} from "react-router-dom";

import { MainContainer } from "./App.style";
import Home from "./Home";
import Sidebar from "./Sidebar";
import ArtistDetails from "./ArtistDetails"



function App() {
  return (
    <MainContainer>
      <Sidebar />

      <Switch>
        <Route path="/details/:id">
          <ArtistDetails />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </MainContainer>
  );
}

export default App;
