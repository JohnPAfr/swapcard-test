import styled from 'styled-components'

export const MainContainer = styled.div`
  display: flex;
  justify-content: center;

  min-height: 100vh;
  height: 100%;
`

export const Button = styled.button`
  padding: 10px 20px;

  border: none;
  border-radius: 50px;

  color: white;
  font-size: 13.3px;
  font-weight: 600;
  background-color: hsl(149, 62%, 33%);

  transition: all 300ms ease-in-out;

  &:hover {
    background-color: hsl(149, 62%, 38%);
  }
`