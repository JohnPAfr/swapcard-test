import React from 'react'
import { useParams } from 'react-router'
import { Link } from 'react-router-dom';
import { useStore } from '../../providers/AppProvider'
import { ArtistDetailsContainer,
  ButtonAddFavorite,
  ButtonBack,
  ButtonRemoveFavorite,
  DetailsLogo,
  DetailsTop,
  DetailsMain, 
  FlexCol,
  DetailsTopContent,
  DetailsReleases} from './ArtistDetails.style';
import Release from './Release';
import { ToastContainer, toast, Slide } from 'react-toastify';

const ArtistDetails = () => {
  const paramsID = useParams().id;
  const { state, dispatch } = useStore();
  const artist = state?.artists?.find(artist => artist.id === paramsID) || state.favorites.find(artist => artist.id === paramsID);
  const logoURL = artist?.fanArt?.backgrounds[0]?.url;
  const rating = artist?.rating?.value || '?';
  const releases = artist?.releases?.edges?.map((release, index) => <Release key={release?.node?.id} release={release} index={index} />);

  const handleFavorite = (action) => {
    if (action === 'add') {
      dispatch({type: 'ADD_FAVORITE', payload: artist});
      toast.success(`${artist?.name} added to your favorites`, {
        position: toast.POSITION.BOTTOM_RIGHT
      });
    } else if (action === 'remove') {
      dispatch({type: 'REMOVE_FAVORITE', payload: artist});
      toast.error(`${artist?.name} removed from your favorites`, {
        position: toast.POSITION.BOTTOM_RIGHT
      });
    }
  }


  return (
    <ArtistDetailsContainer>
      <Link to="/">
        <ButtonBack>Back To Artists</ButtonBack>
      </Link>

      <DetailsTop>
        { logoURL && <DetailsLogo url={logoURL}/> }
        <DetailsTopContent>
          <h1>{artist?.name || 'Artist Not Found'}</h1>
          <div>Rating <span>{rating} / 5</span></div>
        </DetailsTopContent>
      </DetailsTop>
      
      <DetailsMain>
        {
          state?.favorites?.includes(artist)
          ? <ButtonRemoveFavorite onClick={() => handleFavorite('remove')}>Remove From Favorite</ButtonRemoveFavorite>
          : <ButtonAddFavorite onClick={() => handleFavorite('add')}>Add To Favorite</ButtonAddFavorite>
        }
        <DetailsReleases>
          <h2>Releases</h2>
          <div>{releases}</div>
        </DetailsReleases>
      </DetailsMain>

      <ToastContainer 
        autoClose={2500}
        closeButton={false}
        transition={Slide}
      />
    </ArtistDetailsContainer>
  )
}

export default ArtistDetails
