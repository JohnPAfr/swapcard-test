import styled from 'styled-components';

const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
`

export const ArtistDetailsContainer = styled.div`
  position: relative;

  display: flex;
  flex-direction: column;

  width: 100%;
  min-height: 100vh;

  color: white;

  .Toastify__toast--success {
    background-color: hsl(149, 62%, 33%);
  }
  .Toastify__toast--error {
    background-color: hsl(0, 62%, 33%);
  }
`

export const DetailsTop = styled.div`
  display: flex;

  width: 100%;
  height: 340px;

  padding: 32px;

  color: white;
  background: linear-gradient(0deg, rgba(50,50,50,1) 0%, rgba(80,80,80,0.8) 85%);

  & > * {
    align-self: flex-end;
  }
`

export const DetailsTopContent = styled(FlexCol)`
  margin-left: 20px;

  h1 {
    font-size: 84px;
  }

  div {
    padding-bottom: 3px;
    font-weight: 300;
    position: relative;
    &::after {
      content: '';
      position: absolute;
      bottom: 0;
      left: 0;
      width: 20px;
      height: 2px;
      background-color: white;
    }
  }
  span {
    margin-left: 10px;  
    padding: 5px 10px;
    border: 1px solid white;
    border-radius: 50px;
  }
`

export const DetailsLogo = styled.div`
  width: 183px;
  height: 183px;

  border-radius: 100px;
  box-shadow: 0 8px 24px hsla(0, 0%, 0%, 0.3);

  background-image: url(${props => props.url});
  background-position: center;
  background-size: 325.3px 183px;
  background-repeat: no-repeat;
`

export const DetailsMain = styled.div`
  min-height: calc(100% - 340px);
  background-color: hsl(0, 0%, 10%);
`

const Button = styled.button`
  padding: 10px 20px;

  border: none;

  color: white;
  font-size: 13.3px;
  font-weight: 600;
`

export const ButtonBack = styled(Button)`
  position: absolute;
  top: 32px;
  left: 32px;

  background-color: hsl(149, 62%, 33%);

  &:hover {
    background-color: hsl(149, 62%, 38%);
  }
`

const ButtonFavorite = styled(Button)`
  margin: 32px;
  padding: 7px 15px;

  text-transform: uppercase;
  font-size: 12px;
  font-weight: 900;

  border: 1px solid hsl(0, 0%, 60%);
  border-radius: 5px;
  background-color: transparent;

`
export const ButtonAddFavorite = styled(ButtonFavorite)`
  &:hover {
    border: 1px solid hsl(0, 0%, 100%);
  }
`

export const ButtonRemoveFavorite = styled(ButtonFavorite)`
  color: hsl(0, 62%, 50%);
  border: 1px solid hsl(0, 62%, 33%);
  &:hover {
    border: 1px solid hsl(0, 62%, 50%);
  }
`

export const DetailsReleases = styled.div`
  padding-left: 32px;
  padding-right: 32px;
`