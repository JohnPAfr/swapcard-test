import React from 'react'
import { ReleaseContainer } from './Release.style'

const Release = ({ release, index }) => {
  const coverURL = release?.node?.coverArtArchive?.front;
  const title = release?.node?.title;

  return (
    <ReleaseContainer>
      <div>{index}</div>
      <div className="cover" url="http://ia800600.us.archive.org/33/items/mbid-0444b84e-15af-47a4-810a-773258eb9395/mbid-0444b84e-15af-47a4-810a-773258eb9395-17805894563.jpg"></div>
      <div>{title}</div>    
    </ReleaseContainer>
  )
}

export default Release
