import styled from 'styled-components'

export const ReleaseContainer = styled.div`
  display: flex;
  align-items: center;

  margin: 10px 0;
  padding-left: 10px;

  font-size: 14px;

  .cover {
    width: 50px;
    height: 50px;
    margin-right: 20px;

    background-image: url(${props => props.url})
  }

  &:hover {
    background-color: hsl(0, 0%, 15%);
  }

  & > div:first-child {
    color: hsl(0, 0%, 80%);
  }
`