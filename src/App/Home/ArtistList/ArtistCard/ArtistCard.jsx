import React, { useEffect } from 'react'

import { ArtistCardContainer, ArtistName, ArtistCountry, ArtistLogo, ArtistContent } from './ArtistCard.style'

const ArtistCard = ({ artist }) => {
  const logoURL = artist?.fanArt?.backgrounds[0]?.url

  return (
    <ArtistCardContainer to={`/details/${artist?.id}`}>
      { logoURL && <ArtistLogo url={logoURL}/> }
      <ArtistContent>
        <ArtistName>{artist?.name}</ArtistName>
        <ArtistCountry>{artist?.country}</ArtistCountry>  
      </ArtistContent>
      
    </ArtistCardContainer>
  )
}

export default ArtistCard
