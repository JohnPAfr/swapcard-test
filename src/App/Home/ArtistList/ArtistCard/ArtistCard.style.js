import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const ArtistCardContainer = styled(Link)`
  display: flex;
  flex-direction: column;

  width: 215px;
  height: 285px;

  padding: 16px;

  color: white;
  
  background-color: hsl(0, 0%, 11%);
  border-radius: 2px;

  transition: all 200ms ease-in-out;

  &:hover {
    cursor: pointer;
    background-color: hsl(0, 0%, 14%);
  }
`

export const ArtistContent = styled.div`
  margin-top: auto;
  margin-bottom: 10px;
`

export const ArtistName = styled.div`
  font-size: 16px;
  font-weight: 600;
  letter-spacing: 1.1px;
`

export const ArtistCountry = styled.div`
  margin-top: 5px;
  font-size: 12px;
  font-weight: 900;
  color: hsl(0, 0%, 60%);
`

export const ArtistLogo = styled.div`
  width: 183px;
  height: 183px;
  margin: 0 auto;

  border-radius: 100px;
  box-shadow: 0 8px 24px hsla(0, 0%, 0%, 0.3);

  background-image: url(${props => props.url});
  background-position: center;
  background-size: 325.3px 183px;
  background-repeat: no-repeat;
`