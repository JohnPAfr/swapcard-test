import React from 'react'
import { useStore } from '../../../providers/AppProvider'
import ArtistCard from './ArtistCard'
import { ArtistListContainer } from './ArtistList.style'

const ArtistList = () => {
  const { state, dispatch } = useStore();
  
  return (
    <ArtistListContainer>
      {state.artists && Object.entries(state.artists).map((artist) => (
        <ArtistCard key={artist[1].id} artist={artist[1]} />
      ))}
    </ArtistListContainer>
  )
}

export default ArtistList
