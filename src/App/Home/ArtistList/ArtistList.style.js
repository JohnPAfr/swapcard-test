import styled from 'styled-components';

export const ArtistListContainer = styled.div`
  display: flex;
  flex-flow: wrap;
  justify-content: center;

  max-width: 1200px;
  height: calc(100% - 65px);
  min-height: calc(100vh - 65px);

  margin: 0 auto;
  padding-top: 50px;

  & > * {
    margin: 10px;
  }
`