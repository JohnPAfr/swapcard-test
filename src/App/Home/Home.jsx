import React, { useEffect, useState } from 'react'
import { HomeContainer } from './Home.style'
import Searchbar from './Searchbar'
import ArtistList from './ArtistList'
import {
  gql,
  useLazyQuery
} from '@apollo/client'
import { useStore } from '../../providers/AppProvider'
import Loader from 'react-loader-spinner'
import { useParams } from 'react-router'

const GET_ARTISTS = gql`
  query getArtist($query: String!) {
    search {
      artists(query: $query) {
        nodes {
        id
        name
        country
        releases {
          edges {
            node {
              id
              title
              coverArtArchive {
                front
              }
            }
          }
        }
        rating {
          value
        }
        tags {
          nodes {
            name
          }
        }
        fanArt {
          backgrounds {
            url
          }
        }
      }
      }
    }
  }
`

const Home = () => {
  const { state, dispatch } = useStore();
  const [search, setSearch] = useState(state?.search);
  const [getArtists, { loading, data }] = useLazyQuery(GET_ARTISTS);

  const handleSearchChange = e => {
    e.preventDefault();
    setSearch(e.target.value);
  }

  useEffect(() => {
    if (search !== '' && state?.search === search) {
      getArtists({
        variables: { query: search }
      });
    }
    const myArtists = data?.search?.artists?.nodes;
    dispatch({type: 'SAVE_ARTISTS', payload: myArtists});
  }, [data, dispatch, getArtists]);

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    getArtists({
      variables: { query: search }
    });
    dispatch({type: 'SET_SEARCH', payload: search});
  }

  return (
    <HomeContainer>
      <Searchbar handleSearchChange={handleSearchChange} handleSearchSubmit={handleSearchSubmit} searchValue={search}/>
      {
        loading && data === undefined
        ? <Loader type="Rings" color="#aaa" height={100} width={100} style={{width: 'fit-content', margin: '100px auto'}}/>
        : <ArtistList />
      }
    </HomeContainer>
  )
}

export default Home
