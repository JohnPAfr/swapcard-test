import styled from 'styled-components';

export const HomeContainer = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;

  background-color: hsl(0, 0%, 9%);
`