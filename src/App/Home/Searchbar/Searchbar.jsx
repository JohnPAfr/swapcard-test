import React from 'react'
import { SearchButton, SearchContainer, SearchInputContainer } from './Searchbar.style'

const Searchbar = ({ handleSearchChange, handleSearchSubmit, searchValue }) => {
  return (
    <SearchContainer onSubmit={handleSearchSubmit}>
      <SearchInputContainer>
        <input 
          type="text"
          id="search"
          name="search"
          onChange={handleSearchChange}
          value={searchValue}
          placeholder="Find an artist..."/>
      </SearchInputContainer>
      <SearchButton type="submit">Search</SearchButton>
    </SearchContainer>
  )
}

export default Searchbar
