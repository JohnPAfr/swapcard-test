import styled from 'styled-components';
import { Button } from '../../App.style';

export const SearchContainer = styled.form`
  display: flex;
  justify-content: center;
  align-items: center;

  width: 100%;
  height: 65px;

  background-color: hsl(0, 0%, 8%);

  input {
    width: 300px;
    padding: 8px 16px;
    font-size: 14px;

    color: hsl(0, 0%, 10%);
    border-radius: 50px;
    border: none;
  }
`

export const SearchInputContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const SearchButton = styled(Button)`
  padding: 8px 16px;
  font-size: 14px;

  margin-left: 1em;
`