import React from 'react'
import { useStore } from '../../providers/AppProvider'
import { SidebarContainer } from './Sidebar.style'
import SidebarItem from './SidebarItem/SidebarItem';

const Sidebar = () => {
  const { state, dispatch } = useStore();

  const favorites = state?.favorites?.map(favorite => {
    return <SidebarItem key={favorite.id} artist={favorite} />
  })

  return (
    <SidebarContainer>
      <h2>Favorites</h2>
      {favorites}
    </SidebarContainer>
  )
}

export default Sidebar
