import styled from 'styled-components';

export const SidebarContainer = styled.div`
  position: sticky;
  top: 0;
  left: 0;

  display: flex;
  flex-direction: column;

  width: 300px;
  min-height: 100vh;
  height: 100%;
  
  padding-top: 20px;
  
  color: white;
  background-color: hsl(0, 0%, 5%);

  h2 {
    margin-left: 10px;
    margin-bottom: 20px;
    font-size: 1.4rem;
  }
`