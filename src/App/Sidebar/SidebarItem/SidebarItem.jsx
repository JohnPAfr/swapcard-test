import React, { useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import { SidebarItemContainer, SidebarLogo } from './SidebarItem.style'

const SidebarItem = ({ artist }) => {
  const logoURL = artist?.fanArt?.backgrounds[0]?.url;

  return (
    <SidebarItemContainer>
      <Link to={`/details/${artist?.id}`}>
        { logoURL && <SidebarLogo url={logoURL}/> }
        <div>{artist?.name}</div>
      </Link>
    </SidebarItemContainer>
  )
}

export default SidebarItem
