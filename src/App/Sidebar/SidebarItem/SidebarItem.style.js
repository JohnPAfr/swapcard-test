import styled from 'styled-components';

export const SidebarItemContainer = styled.div`
  padding: .6em 1em;
  margin: 0 10px;
  
  border-radius: 5px;

  &:first-child {
    font-size: 14px;
  }

  .muted {
    font-size: 0.6rem;
    font-weight: 500;
  }

  a {
    display: flex;
    align-items: center;

    //color: hsl(0, 0%, 70%);
    opacity: .8;

    transition: all 100ms ease-in-out;
  }

  &:hover a {
    opacity: 1;
    //color: hsl(0, 0%, 95%);
  }
`

export const SidebarLogo = styled.div`
  width: 50px;
  height: 50px;
  
  margin-right: 20px;

  border-radius: 100px;
  box-shadow: 0 8px 24px hsla(0, 0%, 0%, 0.3);

  background-image: url(${props => props.url});
  background-position: center;
  background-size: 88.8px 50px;
  background-repeat: no-repeat;
`