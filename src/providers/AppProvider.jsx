import React, { createContext, useContext, useReducer } from 'react'

const initialState = {
    favorites: JSON.parse(localStorage.getItem('favoriteArtists')) || [],
    artists: [],
    search: ''
}

const store = createContext(initialState);

export const useStore = () => useContext(store);

const reducer = (state, action)  => {
    let newFavorites;
    switch(action.type) {
        case 'SET_SEARCH':
            return {
                ...state,
                search: action.payload
            };
        case "SAVE_ARTISTS":
            return {
                ...state,
                artists: action.payload
            };
        case 'RESET_ARTISTS': 
            return {
                ...state,
                artists: []
            };
        case "ADD_FAVORITE":    
            if (state?.favorites?.find(fav => fav === action.payload))
                return state;
            newFavorites = [...state.favorites, action.payload];
            localStorage.setItem('favoriteArtists', JSON.stringify(newFavorites));
            return {
                ...state,
                favorites: newFavorites
            };
        case "REMOVE_FAVORITE":
            newFavorites = state?.favorites?.filter(favorite => favorite.id !== action.payload.id);
            localStorage.setItem('favoriteArtists', JSON.stringify(newFavorites));
            return {
                ...state,
                favorites: newFavorites
            };
        default:
            return state;
    }
}

export function AppProvider({ children }) {
    const [state, dispatch] = useReducer(reducer, initialState)

    return (
        <store.Provider value={{ state, dispatch }}>
            {children}
        </store.Provider>
    )
}